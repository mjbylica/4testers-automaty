import random
from faker import Faker


def get_random_email(female):
    random_name = Faker()
    if female:
        return f"{random_name.first_name_female().lower()}.{random_name.last_name().lower()}@example.pl"
    else:
        return f"{random_name.first_name_male().lower()}.{random_name.last_name().lower()}@example.pl"


def get_random_employee_data_dictionary():
    # random_email = get_random_email()
    random_number = random.choice(range(1, 41))
    random_boolean = random.choice([True, False])
    return {
        "email": get_random_email(random_boolean),
        "seniority_years": random_number,
        "female": random_boolean
    }


def get_n_length_list_of_employee_data_dictionaries(n):
    list_of_employee_data_dictionaries = []
    for i in range(n):
        list_of_employee_data_dictionaries.append(get_random_employee_data_dictionary())
    return list_of_employee_data_dictionaries


def get_list_of_employee_data_dictionaries_filtered_by_seniority_years(list_of_employee_data_dictionaries):
    filtered_list = []
    for dictionary in list_of_employee_data_dictionaries:
        if dictionary["seniority_years"] >= 10:

            filtered_list.append(dictionary["email"])
    return filtered_list


def get_list_of_employee_data_dictionaries_filtered_by_gender(list_of_employee_data_dictionaries):
    filtered_list = []
    for dictionary in list_of_employee_data_dictionaries:
        if dictionary["female"]:
            filtered_list.append(dictionary)
    return filtered_list


if __name__ == '__main__':
    print(get_random_employee_data_dictionary())
    print((get_n_length_list_of_employee_data_dictionaries(10)))
    print(get_list_of_employee_data_dictionaries_filtered_by_seniority_years(
        get_n_length_list_of_employee_data_dictionaries(10)))
    print(get_list_of_employee_data_dictionaries_filtered_by_gender(get_n_length_list_of_employee_data_dictionaries(10)))
