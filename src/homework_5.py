from datetime import date


# Dodaj do klasy samochodu metody:
# ● drive(distance) -> zwiększa przebieg auta o dystans
# ● get_age() -> zwraca wiek samochodu
# ● repaint(color) -> zmienia kolor samochodu

class Car:
    def __init__(self, brand, color, production_year):
        self.brand = brand
        self.production_year = production_year
        self.color = color
        self.milage = 0
        # self.__is_hungry = True
        # self.__needs_a_walk = False

    def drive(self, distance):
        self.milage = self.milage + distance

    def get_age(self):
        age = date.today().year - self.production_year
        return age

    def repaint(self, color):
        self.color = color

    def __str__(self):
        return f"This is a {self.color} car of a {self.brand} brand and it was made in {self.production_year}."


if __name__ == '__main__':
    big_car = Car("dodge", "black", 2020)
    small_car = Car("toyota", "white", 2021)
    fast_car = Car("ferrari", "red", 2023)

    big_car.repaint("yellow")
    print(big_car)

    print(big_car.get_age())
    big_car.drive(100)
    big_car.drive(300)
    print(big_car.milage)


