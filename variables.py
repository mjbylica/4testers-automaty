friends_name = "Gocha"
friends_age = 39
friends_pet_number = 0
has_driving_license = False
friendship_years = 31.5

print("Name:", friends_name,
      "Age:", friends_age,
      "Number of pets", friends_pet_number,
      "Has driving license", has_driving_license,
      "Friendship years", friendship_years,
      sep="\n")

name_surname = "Marta Byl"
email = "marby@gmail.com"
phone_no = "+22222222"

bio = "Name: " + name_surname + "\nEmail: " + email + "\nPhone: " + phone_no + "\n---------------"
print(bio)

bio_smarter = f"---------------\nName: {name_surname}\nEmail: {email}\nPhone {phone_no}"
print(bio_smarter)

bio_docstring = f"""---------------
Name: {name_surname}
Email: {email}
Phone: {phone_no}"""
print(bio_docstring)



