from src.homework_5 import Car

big_car = Car("dodge", "black", 2020)
small_car = Car("toyota", "white", 2021)
fast_car = Car("ferrari", "red", 2023)


def test_if_car_class_object_is_created():
    assert fast_car.brand == "ferrari"
    assert fast_car.color == "red"
    assert fast_car.production_year == "2023"


def test_default_milage_data_in_car_class_object():
    assert big_car.milage == 0


# def test_sample_car_class_object_data():
#
#     assert print(small_car) == f"This is a {"white"} car of a {"toyota"} brand and it was made in {2021}."

def test_if_drive_method_changes_milage_value():
    big_car.drive(100)
    assert big_car.milage == 100
    big_car.drive(100)
    assert big_car.milage == 200


def test_if_paint_method_changes_color():
    assert small_car.color == "white"
    small_car.repaint("blue")
    assert small_car.color == "blue"

