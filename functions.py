def print_area_of_a_circle_with_radius(r):
    area_of_a_circle = 3.1415 * r ** 2
    print(f" {round(area_of_a_circle)})



if __name__ == '__main__':
    print_area_of_a_circle_with_radius(5)
