def create_email_address(name, surname):
    email = f"{name.lower()}.{surname.lower()}@4testers.pl"
    return email


def get_average_from_list_of_grades(list_of_grades: list):
    return sum(list_of_grades) / len(list_of_grades)


emails = ["a@example.com", "b@example.com"]
if __name__ == '__main__':
    print(create_email_address("Marta", "Bylica"))
    print(get_average_from_list_of_grades([2, 2, 2, 2, 2, 2]))

    print(len(emails))
    print(emails[0])
    print(emails[-1])
